# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

import os
import utils
try:
    import sugsearch    # noqa: F401
except ValueError:
    """Probably already registered."""
import qutenyan         # noqa: F401
import base16           # noqa: F401

os.chdir('/tmp')

# SETTINGS
c.aliases.update({
    'spell-cs': '''set spellcheck.languages "['cs-CZ']"''',
    'spell-en': '''set spellcheck.languages "['en-US']"''',
    'spell-de': '''set spellcheck.languages "['de-DE']"''',
    'spell-es': '''set spellcheck.languages "['es-ES']"''',
    'spell-ru': '''set spellcheck.languages "['ru-RU']"''',
})

c.auto_save.session = True
c.completion.height = "30%"
c.completion.web_history.max_items = 100000
c.content.cookies.accept = 'no-3rdparty'
c.downloads.location.directory = '/tmp'
c.downloads.location.prompt = False
c.downloads.open_dispatcher = 'rifle'
c.editor.command = ['qvim', '{file}', '+normal {line}G{column0}l']

c.hints.auto_follow = 'always'
c.hints.mode = 'number'
c.hints.next_regexes = [rf'\b{word}\b' for word in (
    "next", "older", "more", "continue",
    "→", "≫", "»", "-?>?>",
    "další", "následující", "starší",
)]
c.hints.prev_regexes = [rf'\b{word}\b' for word in (
    "prev(ious)?", "newer", "back",
    "←", "≪", "«", "<<?-?",
    "předchozí", "novější",
)]
c.qt.args = [
    # "ppapi-widevine-path=/usr/lib/chromium/libwidevinecdmadapter.so",
    "ppapi-widevine-path=~/.config/qutebrowser/libwidevinecdmadapter.so",
]
c.session.lazy_restore = True
# /usr/share/qutebrowser/scripts/dictcli.py install \
# cs-CZ en-US de-DE es-ES ru-RU
c.spellcheck.languages = ['en-US']  # , 'en-US', 'de-DE']
c.scrolling.smooth = True
c.tabs.background = True
c.url.auto_search = 'never'

c.url.searchengines = {
    'DEFAULT': 'https://duckduckgo.com/?q={}',
    'd': 'https://duckduckgo.com/?q={}',
    'ddg': 'https://duckduckgo.com/?q={}',
    'g': 'https://www.google.com/search?hl=en&q={}',
    'w': 'https://cs.wikipedia.org/w/index.php?search={}',
    'we': 'https://en.wikipedia.org/w/index.php?search={}',
    'en': 'https://slovnik.seznam.cz/preklad/anglicky/{}',
    'de': 'https://slovnik.seznam.cz/preklad/nemecky/{}',
    'es': 'https://slovnik.seznam.cz/preklad/spanelsky/{}',
    'fr': 'https://slovnik.seznam.cz/preklad/francouzsky/{}',
    'ru': 'https://slovnik.seznam.cz/preklad/rusky/{}',
    'debian': 'https://packages.debian.org/search?keywords={}',
    # 'kol': utils.get_coworkers_addr(),
    'kol': 'http://intranet/search/Stranky/PeopleResults.aspx?k={}',
}

c.window.hide_decoration = True

# base16.set_colors(c, base16.GRUVBOX)

# BINDINGS
# unused: ~!#%^&*()_    e[]a\zxc,    QWETYUI{}ZXCVBMN<>
# REPLACE `:` WITH `;`
config.unbind(':')
for key, command in c.bindings.default['normal'].items():
    if key.startswith(';'):
        config.bind(key.replace(';', ':', 1), command)
        config.bind(key.replace(';', ']', 1), command)
config.bind(';', 'set-cmd-text :')

# MOVING AROUND
scroll_steps, rest_px = utils.get_scroll_vars()
config.bind('<Backspace>', (f'scroll-px 0 {-rest_px} ;;'
                            f'run-with-count {scroll_steps} scroll up'))
config.bind('<Space>', (f'scroll-px 0 {rest_px} ;;'
                        f'run-with-count {scroll_steps} scroll down'))

# TABS
config.bind('t', 'set-cmd-text -s :open -t')
config.bind('b', 'set-cmd-text -rs :buffer')
config.bind('<Ctrl-J>', 'tab-next')
config.bind('<Ctrl-K>', 'tab-prev')
config.bind('ó', 'tab-prev')
config.bind('π', 'tab-next')
for i in range(1, 10):
    config.bind(f'<Ctrl+{i}>', f'buffer {i}')
config.bind('<Ctrl-E>', 'tab-focus -1')
config.bind('<Ctrl-Escape>', 'tab-focus last')

# SEARCHING
config.bind('s', 'set-cmd-text -s :open g')
# config.bind('S', 'set-cmd-text :google \'')
config.bind('S', 'set-cmd-text -s :open -t g')
# config.bind('w', 'set-cmd-text :wikipedia \'')
config.bind('w', 'set-cmd-text -s :open -t we')

# UI
config.unbind('<Ctrl-H>')   # homepage
config.bind('xx', ('config-cycle statusbar.hide ;;'
                   'config-cycle tabs.show always switching'))
config.bind('xb', 'config-cycle statusbar.hide')
config.bind('xt', 'config-cycle tabs.show always switching')
config.bind('xm', 'tab-mute')
config.unbind('M')
config.bind('<Shift-Escape>', 'fake-key <Escape>')  # S-Esc sends Esc directly
config.bind('yl', 'hint --rapid links yank')

# DEV
config.bind('I', 'inspector')

# EXTERNAL
config.bind('ř', 'spawn mpv {url}')
config.bind(':ř', 'hint links spawn mpv {hint-url}')
config.bind(']ř', 'hint links spawn mpv {hint-url}')
config.bind('→', 'spawn --userscript password_fill')
config.bind('<Ctrl-I>', 'open-editor', mode='insert')

# DOWNLOADING
config.bind('ď', 'download-open ;; download-remove')

# GAMEPAD BINDINGS
config.bind('<Ctrl-Left>', 'back')
config.bind('<Ctrl-Right>', 'forward')
config.bind('<Ctrl-Up>', 'tab-close')
config.bind('<Ctrl-Down>', 'set-cmd-text -s :open -t')
config.bind('<Ctrl-Alt-Left>', 'tab-prev')
config.bind('<Ctrl-Alt-Right>', 'tab-next')
config.bind('<Ctrl-Alt-Up>', 'navigate prev')
config.bind('<Ctrl-Alt-Down>', 'navigate next')

# EMACS IN INSERT MODE
config.bind('<Ctrl-B>', 'fake-key <Left>', mode='insert')
config.bind('<Ctrl-F>', 'fake-key <Right>', mode='insert')
config.bind('<Ctrl-A>', 'fake-key <Home>', mode='insert')
config.bind('<Ctrl-E>', 'fake-key <End>', mode='insert')
config.bind('<Ctrl-N>', 'fake-key <Down>', mode='insert')
config.bind('<Ctrl-P>', 'fake-key <Up>', mode='insert')
config.bind('<Ctrl-H>', 'fake-key <Backspace>', mode='insert')
config.bind('<Ctrl-D>', 'fake-key <Delete>', mode='insert')
config.bind('<Ctrl-W>', 'fake-key <Ctrl-Backspace>', mode='insert')
config.bind('<Ctrl-U>',
            'fake-key <Shift-Home> ;; fake-key <Delete>', mode='insert')
config.bind('<Ctrl-K>',
            'fake-key <Shift-End> ;; fake-key <Delete>', mode='insert')

# config.bind('<Ctrl-B>', 'move-to-prev-char', mode='insert')
# config.bind('<Ctrl-F>', 'move-to-next-char', mode='insert')
# config.bind('<Ctrl-A>', 'move-to-start-of-line', mode='insert')
# config.bind('<Ctrl-E>', 'move-to-end-of-line', mode='insert')
# config.bind('<Ctrl-N>', 'move-to-next-line', mode='insert')
# config.bind('<Ctrl-P>', 'move-to-prev-line', mode='insert')
# config.bind('<Ctrl-H>', 'rl-backward-delete-char', mode='insert')
# config.bind('<Ctrl-D>', 'rl-delete-char', mode='insert')
# config.bind('<Ctrl-W>', 'rl-backward-kill-word', mode='insert')
# config.bind('<Ctrl-U>', 'rl-unix-line-discard', mode='insert')
# config.bind('<Ctrl-K>', 'rl-kill-line', mode='insert')
# config.bind('<Ctrl-Y>', 'rl-yank', mode='insert')
