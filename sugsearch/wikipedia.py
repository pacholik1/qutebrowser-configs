import html
import json
import requests
from PyQt5.QtCore import QUrl

import qutebrowser
from qutebrowser.api import cmdutils
from qutebrowser.completion.models import completionmodel

from sugsearch.base import BaseCategory


class WikipediaCategory(BaseCategory):
    name = 'Wikipedia Suggestions'

    def get_suggestions(self, val):
        response = requests.get(
            url='https://en.wikipedia.org/w/api.php',
            params={'action': 'opensearch', 'search': val}
            # 'format': 'json', 'formatversion': '2', 'namespace': '0',
            # 'limit': '10', 'suggest': 'true',
        )
        if response.status_code != 200:
            return
        try:
            data = json.loads(response.content)
            data[1:]
        except Exception:
            return

        for title, summary, url in zip(*data[1:]):
            yield title, summary


def wikipedia_model(*args, info):
    model = completionmodel.CompletionModel(column_widths=(20, 80))
    model.add_category(WikipediaCategory())
    return model


@cmdutils.register(name='wikipedia')
@cmdutils.argument('query', completion=wikipedia_model)
def wikipedia(*query):
    """Search in Wikipedia."""
    query_str = html.escape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://en.wikipedia.org/wiki/{query_str}')
    )
