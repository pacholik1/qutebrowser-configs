from PyQt5.QtCore import QSortFilterProxyModel
from PyQt5.QtGui import QStandardItem, QStandardItemModel

# from qutebrowser.completion.models.completionmodel import CompletionModel


class BaseCategory(QSortFilterProxyModel):
    def __init__(self, delete_func=None, parent=None):
        super().__init__(parent)
        self.delete_func = delete_func
        self.columns_to_filter = [0]
        self.model = QStandardItemModel(parent=self)
        self.setSourceModel(self.model)

    def set_pattern(self, val):
        if not val:
            return

        self.model.clear()
        for row in self.get_suggestions(val):
            self.model.appendRow([QStandardItem(col) for col in row])
